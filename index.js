//console.log("Hello, World");

//-------Addition
function addTwoNumbers(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2 + ":");
	console.log(num1 + num2);
}

//-------Subtraction
function subtractTwoNumbers(num1, num2){
	console.log("Displayed difference of " + num1 + " and " + num2 + ":");
	console.log(num1 - num2);
}

//-------Invoking
addTwoNumbers(5, 15);
subtractTwoNumbers(20, 5);

//-------Multiplication
function multiplyTwoNumbers(num1, num2){
	console.log("The product of " + num1 + " and " + num2 + ":");
	return num1 * num2;
}

//-------Division
function divideTwoNumbers(num1, num2){
	console.log("The quotient of " + num1 + " and " + num2 + ":");
	return num1 / num2;
}

//-------Invoking and Printing
let product = multiplyTwoNumbers(50, 10);
console.log(product);
let quotient = divideTwoNumbers(50, 10);
console.log(quotient);

//-------Area of a Circle
function calculateArea(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	let rSquared = Math.pow(radius, 2);
	return Math.PI * rSquared;
}

let circleArea = calculateArea(15);
console.log(circleArea);

//-------Average
function calculateAverage(num1, num2, num3, num4){
	console.log("The average of  " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":");
	return (num1 + num2 + num3 + num4) / 4;
}

let averageVar = calculateAverage(20, 40, 60, 80);
console.log(averageVar);

//-------Check if passed
function checkIfPassed(score, total){
	console.log("Is " + score + "/" + total + " a passing score?");
	let percentage = (score / total) * 100;
	let isPassed = percentage >= 75;
	return isPassed;
}

let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);
